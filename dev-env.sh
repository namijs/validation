docker build . -t nami-validation-image 
docker tag nami-validation-image jgalazm/nami-validation:$(git rev-parse --short HEAD)
docker push jgalazm/nami-validation:$(git rev-parse --short HEAD)